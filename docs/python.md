# Syntax highlighting

Tabellen Beispiel

| Header 1 | Header 2 | Header 3 |
| -------- | -------: | -------: |
| R11      |      R12 |      R13 |
| R21      |      R22 |      R23 |
| R31      |      R32 |      R33 |



Formel Beispiel:

$$
c = \sqrt{a^2 + b^2}
$$

wobei $\sqrt{}$ die Wurzel bezeichnet

## SQL Code

```sql
SELECT * FROM kunde WHERE nachname LIKE 'S%';
```

![sql2](img/sql2.PNG)

Arbeiten mit Scripts geht einfacher!! Copy Paste in die Konsole.

$$
    c = \sqrt{a^2 + b^2}
$$
inline $x^2$ Wurzel $\cos{(\alpha)}$ cosinus




## Python Code

``` python linenums="10" hl_lines="7"
# This program adds two numbers

num1 = 2.5
num2 = 6.3

# Add two numbers
sum = num1 + num2

# Display the sum
print('The sum of {0} and {1} is {2}'.format(num1, num2, sum))
```

## C Code
```c

#include <stdio.h>
int main()
{
   printf("Hallo Welt");
   return 0;
}
```
## JSON
```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

